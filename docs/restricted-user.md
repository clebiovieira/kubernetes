### Criando o usuário deploy

Esse usuário será usado nas aplicações de CI/CD para executar os deploys no Kubernetes.

- Usuário: `ci-deploys`
- Namespace: `dev`

```shell
./create_user.sh
```

```shell
#!/bin/bash
# 
K8S_USER=ci-deploys
CLUSTER_NAME=k8s-aws
K8S_CLUSTER_API=https://api.k8s.domain.com
#

openssl genrsa -out $K8S_USER.pem 4096
openssl req -new -key $K8S_USER.pem -out $K8S_USER.csr -subj "/O=deploys/CN=$K8S_USER"

cat << EOF > certificate.yaml
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: $K8S_USER
spec:
  groups:
  - system:authenticated
  request: $( cat $K8S_USER.csr | base64 | tr -d '\n' )
  usages:
  - digital signature
  - key encipherment
  - client auth
EOF

kubectl apply -f certificate.yaml
kubectl certificate approve $K8S_USER
kubectl get csr $K8S_USER -o jsonpath='{.status.certificate}' | base64 -d > $K8S_USER.crt

cat << EOF > config
apiVersion: v1
kind: Config
preferences: {}

clusters:
- name: $K8S_CLUSTER_NAME
  cluster:
    insecure-skip-tls-verify: true
    server: $K8S_CLUSTER_API

contexts:
- name: $K8S_CLUSTER_NAME
  context:
    cluster: $K8S_CLUSTER_NAME
    user: $K8S_USER

current-context: $K8S_CLUSTER_NAME

users:
- name: $K8S_USER
  user:
    username: $K8S_USER
    client-certificate-data: $( cat $K8S_USER.crt | base64 -w0 )
    client-key-data: $( cat $K8S_USER.pem | base64 -w0 )
EOF

rm -f $K8S_USER.pem $K8S_USER.crt $K8S_USER.csr 

```


#### Regras de acesso para o usuário deploy com permissão restrita:

> Aplicar em todos os namespaces que o usuário terá permissão.

```yaml
cat <<EOF | kubectl create -f -
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: dev
  name: ci-deploys
rules:
- apiGroups: ["apps", "extensions"]
  resources: ["deployments", "events", "replicasets"]
  verbs: ["get", "list", "patch", "watch", "create", "update"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ci-deploys
  namespace: dev
subjects:
- kind: User
  name: ci-deploys
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role 
  name: ci-deploys
  apiGroup: rbac.authorization.k8s.io
EOF
```

#### Regras de acesso para um usuário cluster-admin com permissão total:

```yaml
cat <<EOF | kubectl create -f -
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ci-deploys
rules:
- apiGroups:
  - '*'
  resources:
  - '*'
  verbs:
  - '*'
- nonResourceURLs:
  - '*'
  verbs:
  - '*'
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ci-deploys
subjects:
- kind: User
  name: ci-deploys
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: ci-deploys
  apiGroup: rbac.authorization.k8s.io
EOF
```
