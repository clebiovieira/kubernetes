# K8S Dashboard

### Gerar certificados e chaves
```shell
openssl genrsa -des3 -passout pass:STRONGPASSWORDHERE -out dashboard.pass.key 2048
openssl rsa -passin pass:STRONGPASSWORDHERE -in dashboard.pass.key -out dashboard.key
openssl req -new -key dashboard.key -out dashboard.csr
openssl x509 -req -sha256 -days 365 -in dashboard.csr -signkey dashboard.key -out dashboard.crt
```

### Criar secret a partir dos certificados gerados
```shell
kubectl create secret generic kubernetes-dashboard-certs --from-file=./ -n kube-system
```

### Criar Dashboard

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/alternative/kubernetes-dashboard.yaml
```

### Aplicação permissões administrativas ao Dashboard (opcional)
```yaml
cat <<EOF | kubectl create -f -
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system

EOF
```

## Nginx Ingress - Autenticação por URL

### Gerar senha
```shell
htpasswd -c auth admin
```

### Criar secret a partir da senha
```shell
kubectl create secret generic basic-auth --from-file=auth -n kube-system
```

### Criar Ingress
```yaml
cat <<EOF | kubectl apply -f -
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: dashboard-auth
  namespace: kube-system
  annotations:
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
spec:
  rules:
  - host: dashboard.domain.com.br
    http:
      paths:
      - path: /
        backend:
          serviceName: kubernetes-dashboard
          servicePort: 80
          
EOF
```
